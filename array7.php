<?php
// membuat array
$user = [
    "Komang",
    "Medi",
    "Usman"
];

// mengisi array pada indek ke-1 ("Evans")
$user[1] = "Evans";

// mencetak isi array
echo "<pre>";
print_r($user);
echo "</pre>";
?>